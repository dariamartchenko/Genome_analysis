#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=unique
#SBATCH --cpus-per-task=30
#SBATCH --mem=12G
#SBATCH --time=0-01:00 # time (DD-HH:MM)

module load samtools
module load sambamba
echo ${1}
/home/shaferab/DEER/sambamba-0.7.0-linux-static view \
--nthreads=30 --with-header --format=bam --show-progress \
-F "mapping_quality >= 1 and not (unmapped or secondary_alignment) and not ([XA] != null or [SA] != null)" \
/home/shaferab/DEER/DATA_ALIGNED_files/${1}.deduped_reads.bam \
-o /home/shaferab/DEER/DATA_ALIGNED_files/${1}.unique_reads.bam &&
/home/shaferab/projects/rrg-shaferab/shaferab/DEER/sambamba-0.7.0-linux-static flagstat \
--nthreads=30 \
/home/shaferab/DEER/DATA_ALIGNED_files/${1}.unique_reads.bam \
> /home/shaferab/DEER/DATA_ALIGNED_files/${1}.unique_reads.flagstat