#mkdir trimmed_slurm
for f in $(ls *.fastq.gz | cut -f1-4 -d'_'| uniq)
do
echo ${f}
sbatch -o /home/shaferab/projects/rrg-shaferab/shaferab/DEER/trimmed_slurm/${f}-%A.out 02_trimmomaticPE.sh ${f}
sleep 10
done
