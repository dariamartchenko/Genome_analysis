#mkdir clean; mkdir clean/clean_slurm; mkdir clean/flagstat; mkdir clean/intervals; cd unique
mkdir clean_slurm
for f in $(ls *.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/shaferab/DEER/DATA_ALIGNED_files/clean_slurm/${f}-%A.out 07_realign.sh ${f}
sleep 10
done