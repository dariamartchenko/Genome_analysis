#MAP_script_RUN
for f in $(ls *.fastq.gz | cut -f1-4 -d'_'| uniq)
do
echo ${f}
sbatch -o /home/shaferab/projects/rrg-shaferab/shaferab/DEER/align_slurm/${f}-%A.out 04_bwa-sortSE.sh ${f}
sleep 10
done
