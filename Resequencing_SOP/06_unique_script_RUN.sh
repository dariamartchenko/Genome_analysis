#unique_RUN.sh
#select only unique mapped reads

#cd /home/shaferab/DEER/DATA_ALIGNED_files
#mkdir unique; mkdir unique/unique_slurm; mkdir unique/flagstat; cd dups
mkdir unique_slurm
for f in $(ls *.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/shaferab/DEER/DATA_ALIGNED_files/unique_slurm/${f}-%A.out 06_unique.sh ${f}
sleep 10
done
