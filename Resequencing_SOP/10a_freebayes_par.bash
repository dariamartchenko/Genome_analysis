#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=freebayes_par
#SBATCH --cpus-per-task=30 # number of MPI process per node
#SBATCH --time=00-12:00

#might choose to request more memory but only used 2% of 500GB on 13 6x deer

#git clone --recursive git://github.com/ekg/freebayes.git
#cd freebayes
#make
#mkdir freebayes_run


cat mpileup_coord.list | \
awk 'FS="\t", OFS="" {print $1,":"$2"-"$3}' | \
parallel -j 30 \
"./freebayes -L bam_list -f wtdgenome1.fasta -r {} -C 2 -3 40 -P 0.0001 -q 20 -W 1,3 -S 4 -M 3 -B 25 -E 3 -v freebayes_run/{}.vcf && \
echo {} is complete"

