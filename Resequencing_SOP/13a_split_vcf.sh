#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=split_vcfs
#SBATCH --time=00-7:00
module load bcftools

#NOTE when I ran this I had already zipped and indexed files so that can be omitted if needed or might still have a bug or two
#ONLY for mpileup, gatk, and freebayes VCFs

VCF_FILE="DEER_gatk_raw.vcf"
bgzip -c $VCF_FILE > $VCFGZ  #compress vcf
tabix -p vcf $VCFGZ  # index compressed vcf
for i in $(tabix --list-chroms $VCF_FILE.gz);
do
bcftools view -r $i $VCF_FILE.gz > ./gatk_chrom/$i.vcf
done

VCF_FILE="DEER_freebayes_raw.vcf"
bgzip -c $VCF_FILE  > $VCFGZ  #compress vcf
tabix -p vcf $VCFGZ  # index compressed vcf
for i in $(tabix --list-chroms $VCF_FILE.gz);
do
bcftools view -r $i $VCF_FILE.gz > ./fb_chrom/$i.vcf
done

VCF_FILE="DEER_raw.vcf"
bgzip -c $VCF_FILE > $VCFGZ  #compress vcf
tabix -p vcf $VCFGZ  # index compressed vcf
for i in $(tabix --list-chroms $VCF_FILE.gz);
do
bcftools view -r $i $VCF_FILE.gz > ./mpileup_chrom/$i.vcf
done
