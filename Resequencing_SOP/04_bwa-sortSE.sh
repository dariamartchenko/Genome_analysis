#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bwa-bam-sort
#SBATCH --cpus-per-task=30
#SBATCH --mem=48G
#SBATCH --time=0-04:00 # time (DD-HH:MM)
module load bwa
module load samtools
#module load sambamba
echo ${1}

bwa mem -M -t 30 \
-R "@RG\\tID:${1}\\tSM:${1}\\tLB:${1}\\tPL:ILLUMINA" \
/home/shaferab/projects/rrg-shaferab/shaferab/DEER/wtdgenome1.fasta \
${1}_trim_R1_001.fastq.gz ${1}_trim_R2_001.fastq.gz | \
samtools sort -@ 30 \
-o /home/shaferab/projects/rrg-shaferab/shaferab/DEER/${1}.sorted_reads.bam &&
/home/shaferab/projects/rrg-shaferab/shaferab/DEER/sambamba-0.7.0-linux-static flagstat \
--nthreads=30 \
 /home/shaferab/projects/rrg-shaferab/shaferab/DEER/${1}.sorted_reads.bam \
> /home/shaferab/projects/rrg-shaferab/shaferab/DEER/${1}.sorted_reads.flagstat
#END
