## Once you have generated your VCF file, you can then move into generating a SFS

## -------------------------------------------------------------------------------------------------------------------------------------------
## Install easySFS from https://github.com/isaacovercast/easySFS
## -------------------------------------------------------------------------------------------------------------------------------------------
## Install miniconda for python3
## Create and activate a new environment: conda create -n easySFS & conda activate easySFS
## Install dependencies: conda install -c bioconda dadi pandas
## Clone this repo
## git clone https://github.com/isaacovercast/easySFS.git
## cd easySFS
## chmod 777 easySFS.py
## ./easySFS.py
## -------------------------------------------------------------------------------------------------------------------------------------------


## -------------------------------------------------------------------------------------------------------------------------------------------
## You popmap.txt should be the same you used before
## your popmapX.txt is two columns seperated by tab in the format 
##      sample_ID <tab> popname
## -------------------------------------------------------------------------------------------------------------------------------------------

## -------------------------------------------------------------------------------------------------------------------------------------------
## Step 1: Identify values for projection

./easySFS.py -i FILENAME.snps.vcf -p popmap.txt --preview

## the output will be in the format (N, S) 
	## N = number of samples in the projection and 
	## S = number of segregaging sites
## copy and paste the output into a spreadsheet and organize by descending order of S
## select the N for the largest S
## -------------------------------------------------------------------------------------------------------------------------------------------


## -------------------------------------------------------------------------------------------------------------------------------------------
## Step 2: Generate SFS

## If you have 3 populations:
./easySFS.py -i FILENAME.snps.vcf -p popmap3.txt -a -f --proj N, N, N

## If you have 2 populations:
./easySFS.py -i FILENAME.snps.vcf -p popmap2.txt -a -f --proj N, N

## If you have 1 population:
./easySFS.py -i FILENAME.snps.vcf -p popmap1.txt -a -f --proj N
## -------------------------------------------------------------------------------------------------------------------------------------------

## -------------------------------------------------------------------------------------------------------------------------------------------
## The SFS will be put into a directory 'Output' containing datadict.txt and two subdirectories
## Within, theree will be subdirectories 'dadi' and 'fastsimcoal2'
## -------------------------------------------------------------------------------------------------------------------------------------------